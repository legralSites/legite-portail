<?php

define('SITE_ENGINE_VERSION','v0.0.1-dev');

//  === dev ====  //
    error_reporting(NULL);ini_set('display_errors', 0);ini_set('display_startup_errors', 0);
    //error_reporting(E_ALL);ini_set('display_errors', 1);ini_set('display_startup_errors', 1);

//  ==== session ==== //
session_name('legite-session');
session_start();
session_regenerate_id();

if (isset($_GET['deconnect'])){
    $_SESSION = array();
    session_destroy();
}


//  === bufferisation ====  //
ob_start();


//  ==== configuration du projet ==== //
//require('../release.php');
require('configLocal/config.php');
//echo __FILE__.__LINE__.'<br>';
//  ==== constantes generales ==== //
//define('DATENOW',date('Y/m/d'));
$protocole=(isset($_SERVER["HTTPS"]))?'https://':'http://';
define('ARIANE_URI',$_SERVER["REQUEST_URI"]);
define('ARIANE_FULL',$protocole.$_SERVER["SERVER_NAME"].ARIANE_URI);
unset($protocole);



// = ======================= = //
// = gestion du mot de passe = //
// = ======================= = //
/* $P_ETAT:etat du password
 * -1: non demandé
 * 0 demander non fournis
 * 1: fournis mais incorrect
 * 2: fournis ok
 */
$P_ETAT=-1; // par defaut pas de mot de passe demandé

/* $isPassAsk: etat de la demande du password, demande faite par le routeur (ensemble ou une page)
 * 0: pas de demande de pawwd
 * 1: demande faite
 */
$isPassAsk=0;// 1: si un routeur a demander un mot de passe (permet d'activer un passwd general

//  === recuperation du mdp ====  //
$P_GIVE=NULL;
if(isset($_POST['p'])){$P_GIVE=$_POST['p'];}
elseif (isset($_SESSION['p'])){$P_GIVE=$_SESSION['p'];}
define ('P_GIVE',$P_GIVE);
$_SESSION['p']=$P_GIVE;
unset($P_GIVE);


//  === mot de passe globale requis ====  //
//if (P_GIVE !== PWD ){$isPassAsk=1;}

//  ===================  //
//  === Librairies ====  //
//  ===================  //
require('vendors/legral/gestLib/gestLib.php');

$gestOut->setIsReturn(0);
$gestOut->setIsFile(1);
$gestOut->writeHeader('<!DOCTYPE html><html lang="fr"><head><meta charset="utf-8" />');
$gestOut->writeHeader('<link rel="stylesheet" href="https://legral.fr/lib/legral/php/gestLib/gestLib.css">');
$gestOut->writeHeader('</head>');
$gestOut->writeDate();
/*
if (DEV_LVL>1){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $gestOut->setIsReturn(1);
}
*/
//echo __FILE__.':'.__LINE__;
require('modeles/_modeleMVC/dev.php');
define('MINIMIZE_SUFFIXE',DEV_LVL>0?'':'.min');

require('modeles/_modeleMVC/templates.php');

// - gestLogins - //
require('vendors/legral/gestLogins/sid/gestLogins.php');
$legralUsers=new GestLogins(PROJET_NOM.'_');
include('configLocal/logins.php');
include('modeles/_modeleMVC/logins.php');

require('vendors/legral/cachePSP/sid/cachePSP.php');
$cachePSP=new CachePSP();
$cachePSP->setCacheRoot(CACHE_ROOT);
$cachePSP->setUtilisateur(LOGIN);

// = ======== = //
// = dataBase = //
// = ======== = //
// geré par le controleur

//echo __FILE__.__LINE__.'<br>';
// = ========= = //
// = modeleMVC = //
// = ========= = //

// - route - //
$route=ROUTE_DEFAUT;
$routeurFile=ROUTE_DEFAUT.'.php';

$controleurFile=CONTROLEURFILE_DEFAUT;//controleur par default



$pageBase=PAGE_ROOT;
$isPageExist=1;
$pageDefault=PAGE_DEFAUT;
$pageAsk=PAGE_DEFAUT;  // page demander (code GET)
$pagePath=PAGE_DEFAUT; // chemin (relatif ou absolu) + nom +(NOT ext) du fichier a charger
$pageExt=PAGE_EXT_DEFAUT;

$titleHTML=TITLE_HTML_DEFAUT; //titleHTML

// - restauration par SESSION - //
// -> INCOMPATIBLE AVEC LE SYSTEME DE CACHE
//if (isset($_SESSION['redir_route']))$route=$_SESSION['redir_route'];
//if (isset($_SESSION['redir_page']))$pageAsk=$_SESSION['redir_page'];


// = ======= = //
// = routeur = //
// = ======= = //
// - Selection du routeur par priorité (surcharge par GET)- //
if (isset($_GET[PROJET_NOM])){$route=PROJET_NOM;  $pageAsk=$_GET[PROJET_NOM];}
//elseif (isset($_GET['serveur'])){$route='serveur';  $pageAsk=$_GET['serveur'];}
//elseif (isset($_GET['typo']))   {$route='typo';     $pageAsk=$_GET['typo'];}
//elseif (isset($_GET['legralLibs']))   {$route='legralLibs';     $pageAsk=$_GET['legralLibs'];}
//elseif (isset($_GET['modeleMVC-exemple'])){$route='modeleMVC-exemple';    $pageAsk=$_GET['modeleMVC-exemple'];}
elseif (isset($_GET['_modeleMVC'])) {$route='_modeleMVC';     $pageAsk=$_GET['_modeleMVC'];}


// - constantes (pré-route) - //
define ('ROUTE',$route);
define ('ROUTEUR_FILE',ROUTE.'.php');
define ('PAGE',$pageAsk);   // avant le routeur
unset($route,$pageAsk);
//echo __FILE__.':'.__LINE__.'route='.ROUTE.'<br>';

$pageBase=PAGE_ROOT.ROUTE.'/';
require('routeurs/'.ROUTE.'/'.ROUTEUR_FILE);
//echo __FILE__.':'.__LINE__.'route='.ROUTE.'<br>';

// = ============================== = //
// = gestion des pages inexistantes = //
// = ============================== = //
define ('PAGE_EXIST',$isPageExist);unset($isPageExist);
define ('FICHIER_PATH',$pageBase.$pagePath.$pageExt);
define ('FICHIER_EXIST',file_exists(FICHIER_PATH));

if (PAGE_EXIST === 0       // -- detecté par le routeur -- //
 OR FICHIER_EXIST === FALSE// -- fichier physiquement inexistant -- //
){
    // surcharger la page '_pageNoExist' defini par le routeur
    $pageBase=PAGE_ROOT;
    $pagePath='_modeleMVC/_pageNoExist';
    $pageExt='.php';
}

define ('PAGE_BASE',$pageBase);

// - constantes (post-route) - //
define ('CONTROLEUR_FILE',$controleurFile);
define ('PAGE_PATH',$pagePath);
define ('PAGE_EXT',$pageExt);

define('TITLEHTML',($titleHTML===NULL?TITLE_HTML_DEFAUT:$titleHTML));


unset($titleHTML,$route,$routeurFile,$controleurFile,$pageBase,$pageDefault,$pageAsk,$pagePath,$pageExt,$ariane,$pspV);

// - si pas de password demandé: sauvegarde de la route et de la page  - //
if ($P_ETAT === -1){
    $_SESSION['redir_route']=ROUTE;
    $_SESSION['redir_page']=PAGE;
}


// = ========== = //
// = controleur = //
// = ========== = //
if (CONTROLEUR_FILE !== NULL and file_exists('controleurs/'.CONTROLEUR_FILE))require('controleurs/'.CONTROLEUR_FILE);

// (apres le controleur configUser)
//define('VIDEOLEGRAL_FLUX',file_get_contents(VIDEOLEGRAL_FILE));

// = ==== = //
// = vues = //
// = ==== = //
try{
    switch(ROUTE){

    default:
            if (PAGE === 'export')
                {include (PAGE_BASE.ROUTE.'/export.php');/* - format special pas de HTML - */}
            else{
                if ($cachePSP->showCache()===1){
                    // - ajout du code JS, en fin de page indiquant la duree de construction de la page (incluant le cache) - //
                    define('PAGE_DUREE_CONSTRUCTION',microtime(TRUE)-SCRIPTDEBUT);
                    $js='<script>/*ajouter par index.php*/';
                    $js.='poId=document.getElementById("cacheInfos");';
                    $js.='var cachePSP_pageDureeContruction='.PAGE_DUREE_CONSTRUCTION.';'; // code integrer au cache
                    $js.='var gain=cachePSP_cacheDureeContruction-cachePSP_pageDureeContruction;';
                    $js.='var ratio=100*cachePSP_pageDureeContruction/cachePSP_cacheDureeContruction;';

                    $js.='if ( poId != undefined){';
                    $js.='poId.innerHTML+="Page générée en '.number_format(PAGE_DUREE_CONSTRUCTION,6).'s.";';
                    $js.='poId.innerHTML+=" Cache contruit en:"+cachePSP_cacheDureeContruction.toFixed(6)+"s.";';
                    $js.='poId.innerHTML+=" Gain: "+gain.toFixed(6)+"s";';
                    $js.='poId.innerHTML+=" Ratio: "+ratio.toFixed(3)+"%";';
                    $js.='}</script>'."\n";
                    echo $js;
                }
                else{// pas de cache: on construit
                    //echo __FILE__.':'.__LINE__.'<br>';
                    require (PAGE_ROOT.'legite/index.php');
                }
            }
    }//switch(ROUTE)

    // - sauvegarde du cache et affochage du buffer - //
    $cachePSP->saveCache();
    ob_end_flush();



}catch (Exception $e){

    if(DEV_LVL>0)echo gestLib_inspect('$e',$e);
    echo 'Exception reçue : ',  $e->getMessage(), "\n";
}

/*
//  ==== lien https ==== //
$host  = $_SERVER['HTTP_HOST'];
$uri   = $_SERVER['REQUEST_URI'];
$pspExtra = '&';
define ('HTTPS_LINK','https://'.$host.'/'.$uri);
*/
