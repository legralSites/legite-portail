<!DOCTYPE html><html lang="fr">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <meta name="robots" content="index,follow">
    <meta name="author" content="Pascal TOLEDO">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="generator" content="vim">
    <meta name="identifier-url" content="http://legral.fr">
    <meta name="date-creation-yyyymmdd" content="20181101">
    <meta name="date-update-yyyymmdd" content="20181101">
    <meta name="category" content="">
    <meta name="publisher" content="legral.fr">
    <meta name="copyright" content="pascal TOLEDO">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Le G.I.T.E. - portail</title>

    <script src="web/locales/scripts<?php echo MINIMIZE_SUFFIXE?>.js"></script>
    <link rel="stylesheet" href="web/locales/styles<?php echo MINIMIZE_SUFFIXE?>.css" media="all">
<?php
if(TEMPLATE!==TEMPLATE_DEFAUT){
    echo'<link rel="stylesheet" href="web/css/legite/'.TEMPLATE.'.css" media="all">';
}
?>
</head>

<body>

<nav id="menu_flottant">
    <!--
    <a href="#Communiquer">Communiquer avec le monde</a><br>
    <a href="#Dev">Dev</a><br>
    -->
</nav>

<div id="page">

    <header>
        <div id="headerGauche">

        </div>

        <h1><a href="?">le G.I.T.E. - Portail</a></h1>

        <nav>
            <span title="nouvelle fenetre">Nos outils de communications:</span>
          - <a target="forum" href="/forums/legite">forum</a>
          - <a target="tchat" href="https://webchat.freenode.net?channels=legite">tchat</a>
          - <a target="piwigo"    href="/piwigo/">album photos</a>
          - <a target="videoassistance"    href="https://meet.jit.si/legite.org">vidéo assistance</a>
          -
        </nav>

        <nav>
            nos services:
          - <a href="?legite=services#creaSite">création de Site Internet</a>
          - <a href="?legite=services#depanInfo">D&eacute;pannage Informatique</a>
          - <a href="?legite=services#Bricolage">Bris-colle-Heurt</a>
          -
        </nav>
        <!--
        <nav>
            nos partenaires:       
          - <a target="exterieur" href="http://coagul.org/drupal/" title="LUG(Linux User group) de Dijon" >coagul.org</a>
          - <a target="exterieur" href="https://dijontech.fr">Dijon Tech</a>
        </nav>
        -->

        <div id="headerDroit">
<?php include './menus/_modeleMVC/menuSelect.php';?>
        </div>
    </header><!-- header -->

<?php
// - notification - //
if ($legralUsers->getEtat()===-1){
    echo '<div>Mauvais login/mot de passe</div>';
}

    //include 'vues/default/thematiques.php';
    include 'vues/'.ROUTE.'/'.PAGE.PAGE_EXT;
    include PAGE_ROOT.'_modeleMVC/footer.php';

?>
</div><!-- //page -->