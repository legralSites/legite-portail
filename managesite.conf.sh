#!/bin/sh
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier


#############
# variables #
# surcharge #
#############
outFileCSS='styles';
outFileJS='scripts';


##################
#   creation de  #
# projet ou site #
##################
createProjet(){
	createModeleMVC
}

createModeleMVC(){
    echo ""
    evalCmd "git init"
    echo ""
    echo "Editer managesite.conf.sh" 
    echo "managesite.sh --link --min"
    echo ""
}



#######################################
# linkFiles()                         #
# link (ou copie) des fichiers (lib)  #
#######################################
linkFiles(){
	echo "$INFO # - managesite.conf.sh:linkFiles() - #$NORMAL";

    # -- creation des repertoires -- #
    evalCmd "mkdir vendors";
    evalCmd "mkdir vendors/tiers";
    #evalCmd "mkdir vendors/legral"; #rep lier
    
    evalCmd "mkdir web/"; # racine css et js
    evalCmd "mkdir web/js web/css"; # modif js et css du projet et surcharges des lib
    evalCmd "mkdir web/css-vendors"; # css tiers
    evalCmd "mkdir web/js-vendors"; # js tiers
    evalCmd "mkdir web/locales" # fichiers css/js conatenner et minimiser


    # -- CSS/JS  -- #
    echo "";
    # -- css/js-legral -- #
    echo "$INFO lier les repertoires CSS/JS de source legral $NORMAL";
    lier /www/sites-dev/lib/legral/css ./web/css-legral;
    lier /www/sites-dev/lib/legral/js  ./web/js-legral;
    # -- css/js: tiers -- #
    echo "$INFO lier les repertoires CSS/JS de source tiers $NORMAL";
    lier /www/sites-dev/lib/tiers/css ./web/css-vendors;
    lier /www/sites-dev/lib/tiers/js  ./web/js-vendors;


    # --- css: specifique au projet --- #
    #normalement rien a lier

    echo "$INFO # ---- librairies: PHP --- # $NORMAL";
    lier /www/sites-dev/lib/legral/php	./vendors/legral;
    lier /www/sites-dev/lib/tiers/php	./vendors/tiers;


    #################################
    # - lier des ROUTES externes  - #
    #################################
    echo "";
	echo "$INFO # -- lier des ROUTES externes  -- # $NORMAL";
    evalCmd "mkdir -p menus/ routeurs/ controleurs/ modeles/ vues/"
    evalCmd "mkdir -p web/img/"

    siteVersion="sid"    # - version du site source - #
    siteVersion=""    # - version du site source - #
    
    route='_modeleMVC';
    lier /www/sites-dev/_modeleMVC/$siteVersion/routeurs/$route     routeurs/$route; #1 fichier
    lier /www/sites-dev/_modeleMVC/$siteVersion/controleurs/$route  controleurs/$route;  #repertoire
    lier /www/sites-dev/_modeleMVC/$siteVersion/modeles/$route      modeles/$route;  #repertoire
    lier /www/sites-dev/_modeleMVC/$siteVersion/menus/$route        menus/$route;  #repertoire
    lier /www/sites-dev/_modeleMVC/$siteVersion/vues/$route         vues/$route;  #repertoire
    lier /www/sites-dev/_modeleMVC/$siteVersion/web/img/$route      web/img/$route;  #repertoire

    
    route='serveur';
#    echo "$route"
#    lier /www/sites-dev/_modeleMVC-extras/$siteVersion/routeurs/$route       routeurs/$route; #1 fichier
#    lier /www/sites-dev/_modeleMVC-extras/$siteVersion/controleurs/$route  controleurs/$route;  #repertoire
#    lier /www/sites-dev/_modeleMVC-extras/$siteVersion/menus/$route        menus/$route;  #repertoire
#    lier /www/sites-dev/_modeleMVC-extras/$siteVersion/vues/$route         vues/$route;  #repertoire

    route='typo';
#    echo "$route"
#    lier /www/sites-dev/_modeleMVC-extras/$siteVersion/routeurs/$route       routeurs/$route; #1 fichier
   #lier /www/sites-dev/_modeleMVC-extras/$siteVersion/controleurs/$route  controleurs/$route;  #repertoire
#    lier /www/sites-dev/_modeleMVC-extras/$siteVersion/menus/$route        menus/$route;  #repertoire
#    lier /www/sites-dev/_modeleMVC-extras/$siteVersion/vues/$route         vues/$route;  #repertoire

#    route='legralLibs';
#    echo "$route"
#    lier /www/sites-dev/_modeleMVC-extras/$siteVersion/routeurs/$route       routeurs/$route; #1 fichier
#    lier /www/sites-dev/_modeleMVC-extras/$siteVersion/menus/$route        menus/$route;  #repertoire
    #lier /www/sites-dev/_modeleMVC-extras/$siteVersion/vues/$route        vues/$route;  #repertoire

#    echo""
#    lier   /www/sites-dev/_modeleMVC-extras/$siteVersion/vues/gestCycles     vues/gestCycles;  #repertoire
#    lier   /www/sites-dev/_modeleMVC-extras/$siteVersion/vues/gestClasseurs  vues/gestClasseurs;  #repertoire
    unset $route;


    ################################
    # - Changement des droits  - #
	echo "";
	echo "$INFO Changement des droits $NORMAL";
	f='./';	cmd="chmod -R 755 $f"; evalCmd "$cmd";
	#f='./';	cmd="chown -R pascal:www-data $f"; evalCmd "$cmd";
}


#########################################################
# local_concatCSS|JS()                                  #
# concatenne les fichiers css dans ./styles/styles.css  #
# concatenne les fichiers js  dans ./locales/scripts.js #
#########################################################
# - concatenation des fichiers css - #
local_concateCSS() {
    echo "$couleurINFO # - concatenation des fichiers css via concateCSS - #$couleurNORMAL";

#   f='./web/css-legral/knacss/knacss-V5.0.1.css';		    evalCmd "concateCSS $f";
    f='./web/css-legral/html/mvc1.css';		                evalCmd "concateCSS $f";
    f='./web/css-legral/menuStylisee/ms.css';		        evalCmd "concateCSS $f";

    f='./web/css-legral/notes/notesMVC.css';       	        evalCmd "concateCSS $f";
    f='./web/css-legral/tutoriels/tutorielsMVC.css';        evalCmd "concateCSS $f";
    
    # - feuille de style de librairie - "
    f='./web/css-legral/gestLib/gestLib.css';               evalCmd "concateCSS $f";
    f='./web/css-legral/notifs/notifs.css';		            evalCmd "concateCSS $f";
    #f='./web/css-legral/gestClasseurs/gestClasseurs.css';   evalCmd "concateCSS $f";

    # - Portail - #
    f='./web/css/portail/portail.css';   evalCmd "concateCSS $f";

} #local_concateCSS


# - concatenation des fichiers js - #
local_concateJS() {
    echo "$couleurINFO # - concatenation des fichiers js via concateJS - #$couleurNORMAL";
    #echo "$couleurWARN Pas de lib js à concatener $couleurNORMAL";return 1;	# a commenter une fois parametré

    # - librairies legral: js  - #
    f='./web/js-legral/gestLib/gestLib.js';                 evalCmd "concateJS $f";
    f='./web/js-legral/modeleMVC/modeleMVC.js';             evalCmd "concateJS $f";

    f='./web/js-legral/cookies/cookies.js';                 evalCmd "concateJS $f";
    #f='./web/js-legral/gestBufCirc/gestBufCirc.js';         evalCmd "concateJS $f";
    #f='./web/js-legral/gestClasseurs/gestClasseurs.js';     evalCmd "concateJS $f";
    #f='./web/js-legral/gestCycles/gestCycles.js';           evalCmd "concateJS $f";
    #f='./web/js-legral/gestionScenario/gestionScenario.js'; evalCmd "concateJS $f";
#    f='./web/js-legral/gestAncres/gestAncres.js';           evalCmd "concateJS $f";



    # - librairies tiers: js  - #
    #f='./web/js-legral/crypt/md5-v2.2.min.js';              evalCmd "concateJS $f";
    #f='./web/js-legral/crypt/SHA-1-v2.2.min.js';            evalCmd "concateJS $f";

    # - librairies du projet: js  - #

} #local_concateJS


