<?php
if (DEV_LVL >2){echo __FILE__.' page:'.PAGE.'<br>';}
// - valeur par defaut de la route - //
//$controleurFile=ROUTE.'.php';
$controleurFile=NULL;

$titleHTML=ROUTE;
$pageExt='.php';

// - variable interne - //
$isPageExist=1;// par defaut la page existe

// - routage de la page - //
switch (PAGE){
    case 'thematiques':
        $pagePath=PAGE;
        $titleHTML='legite.org - Portail des thématiques';
        break;
    
    case 'configUser':
        //if (DEV_LVL >2){echo 'page:configUser<br>';}
        $controleurFile=ROUTE.'/configUser.php';
        $pagePath=PAGE;
        $cachePSP->setPause();
        $titleHTML='legite.org - Configuration et Profil de l\'utilisateur';
        break;

    default: // PAGE inexistante
        $isPageExist=0;
        $pagePath='_pageNoExist';
        $pageExt='.html';
}// switch (PAGE)

